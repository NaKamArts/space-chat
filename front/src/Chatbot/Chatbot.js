// Chatbot.js
import React, { useState, useRef, useEffect } from "react";
import axios from "axios";
import "./Chatbot.css";

const Chatbot = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [userInput, setUserInput] = useState("");
  const [chatMessages, setChatMessages] = useState([]);

  const toggleChat = () => {
    setIsOpen(!isOpen);
  };

  const handleInputChange = (e) => {
    setUserInput(e.target.value);
  };

  const handleSendMessage = () => {
    if (!userInput) return;

    // Message de l'utilisateur immédiat
    setChatMessages((prevMessages) => [
      ...prevMessages,
      { text: userInput, type: "user" },
    ]);
    setUserInput("");

    // Délai avant d'envoyer la requête au serveur
    setTimeout(() => {
      axios
        .post("http://127.0.0.1:8000/api/ask", {
          lang: "fr", // changer la langue ici et prendre la langue du navigateur
          question: userInput
        })
        .then((response) => {
          // Délai avant d'ajouter la réponse du bot
          setTimeout(() => {
            setChatMessages((prevMessages) => [
              ...prevMessages,
              { text: response.data.response, type: "bot" },
            ]);
          }, 1000); // ajuster le délai selon vos besoins
        })
        .catch((error) => {
          // Délai avant d'ajouter le message d'erreur du bot
          setTimeout(() => {
            setChatMessages((prevMessages) => [
              ...prevMessages,
              { text: "Une erreur est survenue", type: "bot" },
            ]);
          }, 1000); // ajuster le délai selon vos besoins
        });
    }, 0); // ajuster le délai avant d'envoyer la requête selon vos besoins
  };

  const chatContainerRef = useRef(null);
  useEffect(() => {
    // défiler automatiquement vers le bas à chaque mise à jour du contenu
    if (chatContainerRef.current) {
      chatContainerRef.current.scrollTop =
        chatContainerRef.current.scrollHeight;
    }
  }, [chatMessages]);

  return (
    <div className={`chatbot-container ${isOpen ? "open" : ""}`}>
      <div className="chatbot-icon" onClick={toggleChat}>
        {/* Icône du chatbot */}
        <img src="images/chat-bot.png" alt="Chatbot" className="chatbot-svg" />
      </div>

      {isOpen && (
        <div className="chatbot-content">
          <div className="chat-messages" ref={chatContainerRef}>
            {chatMessages.map((message, index) => (
              <div
                key={index}
                className={`chat-message ${
                  message.type === "bot" ? "bot-content" : "user-content"
                }`}
              >
                {message.text}
              </div>
            ))}
          </div>
          {/* Champ de saisie et bouton d'envoi */}
          <div className="bottom-bar">
            <input
              type="text"
              value={userInput}
              onChange={handleInputChange}
              placeholder="Saisissez votre message..."
              className="input-field"
            />
            <button onClick={handleSendMessage} className="button button1">
              Envoyer
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Chatbot;
