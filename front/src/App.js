// App.js
import React from "react";
import Chatbot from "./Chatbot/Chatbot";
import "./App.css";

const App = () => {

  return (
    <div>
      {/* Votre contenu principal */}
      <h1>Mon site web</h1>
      
      {/* Chatbot */}
      <Chatbot />
    </div>
  );
};

export default App;