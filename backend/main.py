from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from routes.ask import router as ask_router
from utils.error_handler import handle_error

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


@app.exception_handler(RequestValidationError)
async def error_handler(request, exc):
    return handle_error(exc)


@app.exception_handler(Exception)
async def error_handler(request, exc):
    return handle_error(exc)


@app.exception_handler(HTTPException)
async def error_handler(request, exc):
    return handle_error(exc)

# Including routers here
app.include_router(ask_router, prefix="/api/ask", tags=["ask"])


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/api")
async def api_root():
    return {"message": "api root"}

