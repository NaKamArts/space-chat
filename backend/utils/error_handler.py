from fastapi import HTTPException
from fastapi.exceptions import RequestValidationError


def handle_error(error):
    if isinstance(error, HTTPException):
        raise HTTPException(status_code=error.status_code, detail=error.detail)
    if isinstance(error, RequestValidationError):
        raise HTTPException(status_code=422, detail=error.errors()[0]["msg"])
    if isinstance(error, Exception):
        raise error
