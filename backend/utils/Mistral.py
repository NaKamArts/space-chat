import os
from dotenv import load_dotenv
from mistralai.client import MistralClient
from mistralai.models.chat_completion import ChatMessage

#Create Mistral Client
load_dotenv
api_key = os.environ.get("MISTRAL_API_KEY")
client = MistralClient(api_key=api_key)
model = "mistral-tiny"

def Mistral_Chat(prompt):
    response = client.chat(
        model = model,
        messages = [ChatMessage(role="user", content=prompt)] ,
    )
    return response.choices[0].message.content #return str