from unidecode import unidecode

from utils.error_handler import handle_error

fr_words = {
    "next_launch": ["prochain", "decollage", "lancement", "fusee"],
    "iss_position": ["ou", "position", "iss", "station"],
    "tesla_position": ["ou", "position", "tesla", "roadster"],
    "starlink_position": ["nombre", "starlink", "starlinks", "satellite", "satellites"]
}
en_words = {
    "next_launch": ["next", "launch", "rocket"],
    "iss_position": ["where", "position", "iss", "station"],
    "tesla_position": ["where", "position", "tesla", "roadster"],
    "starlink_position": ["number", "starlink", "starlinks", "satellite", "satellites"]
}

separators = [",", '?', ';', ':', "'"]


"""
This function will check if the question contains enough words to redirect to predefined responses
"""


def check_words(lang, question):
    try:
        # Checking for english questions
        if lang == "en":
            # Counters to determine purpose of the question
            next_launch_words = 0
            iss_position_words = 0
            tesla_position_words = 0
            starlink_position_words = 0

            # Replace separators by spaces
            for separator in separators:
                question = question.replace(separator, " ")

            # Flatten all uppercases
            question = question.lower()

            # Splitting question words
            splitted_question = question.split(" ")
            # Appending an index on array, in case of a needed word is in last position
            splitted_question.append("?")

            for word in splitted_question:
                # Breaking if count is enough
                if next_launch_words == 2:
                    return "next_launch"
                if iss_position_words == 2:
                    return "iss_position"
                if tesla_position_words == 2:
                    return "tesla_position"
                if starlink_position_words == 2:
                    return "starlink_position"
                # Counting words
                if word in en_words["next_launch"]:
                    next_launch_words += 1
                if word in en_words["iss_position"]:
                    iss_position_words += 1
                if word in en_words["tesla_position"]:
                    tesla_position_words += 1
                if word in en_words["starlink_position"]:
                    starlink_position_words += 1

        # Checking for french questions
        if lang == "fr":
            # Counters to determine purpose of the question
            next_launch_words = 0
            iss_position_words = 0
            tesla_position_words = 0
            starlink_position_words = 0

            # Replace separators by spaces
            for separator in separators:
                question = question.replace(separator, " ")

            # Flatten all uppercases or accents
            question = question.lower()
            question = unidecode(question)

            # Splitting question words
            splitted_question = question.split(" ")
            # Appending an index on array, in case of a needed word is in last position
            splitted_question.append("?")

            for word in splitted_question:
                # Breaking if count is enough
                if next_launch_words == 2:
                    return "next_launch"
                if iss_position_words == 2:
                    return "iss_position"
                if tesla_position_words == 2:
                    return "tesla_position"
                if starlink_position_words == 2:
                    return "starlink_position"
                # Counting words
                if word in fr_words["next_launch"]:
                    next_launch_words += 1
                if word in fr_words["iss_position"]:
                    iss_position_words += 1
                if word in fr_words["tesla_position"]:
                    tesla_position_words += 1
                if word in fr_words["starlink_position"]:
                    starlink_position_words += 1
        return False
    except BaseException as e:
        return handle_error(e)
