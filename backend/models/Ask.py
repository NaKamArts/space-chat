from pydantic import BaseModel

"""
Ask model

This is the body on an incoming user question
lang: str -> the lang the user used to ask the question, it will be usefull later
question: str -> the question the user asked
"""


class Ask(BaseModel):
    lang: str
    question: str
