from pydantic import BaseModel

"""
BotResponse model

This is the content of the response of the bot
token: str -> the token of the user, optional because it is only usefull the first time to send it to the user
response: str -> the response of the bot
"""


class BotResponse(BaseModel):
    token: str = None
    response: str
